# Chatlio Portal for Jira Service Desk Documentation

### Installation

Install the plugin through the atlassian marketplace.

### Configuration

To configure the support portal with your Chatlio widget click on the `Chatlio Portal` link in the `Other` section from the Add-ons page.

From the settings page you will be able to add your Chatlio Widget ID, which can be found on your Chatlio account dashboard.

After adding your Widget ID and saving the settings, your Chatlio widget will be displayed on the support portal.

### Disable It

To disable the chat window simply browse back to the settings page, remove the Widget ID and click save.

